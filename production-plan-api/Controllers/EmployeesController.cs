﻿using production_plan_api.Models.DTO;
using production_plan_api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace production_plan_api.Controllers
{
    public class EmployeesController : ApiController
    {
        [HttpGet]
        [ActionName("getEmployees")]
        [Route("api/getEmployees")]
        public async Task<HttpResponseMessage> getEmployees()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<EmployeeDto> resultData = null;
            try
            {
                using (EmployeeRepo e = new EmployeeRepo())
                {
                    resultData = await e.GetAllEmployees();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("getFilterList")]
        [Route("api/employees/getFilterList")]
        public async Task<HttpResponseMessage> getFilterList()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<FilterDto> resultData = null;
            try
            {
                using (EmployeeRepo e = new EmployeeRepo())
                {
                    resultData = await e.GetFilterList();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("getEmployeesNoUsername")]
        [Route("api/getEmployeesNoUsername")]
        public async Task<HttpResponseMessage> getEmployeesNoUsername()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<EmployeeDto> resultData = null;
            try
            {
                using (EmployeeRepo e = new EmployeeRepo())
                {
                    resultData = await e.GetAllEmployees(false);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        //[ActionName("getEmployee")]
        //[Route("api/getEmployee")]
        public async Task<HttpResponseMessage> get(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<EmployeeDto> resultData = null;
            try
            {
                using (EmployeeRepo e = new EmployeeRepo())
                {
                    resultData = await e.GetEmployeeRecord(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPost]
        //[ActionName("postEmployee")]
        //[Route("api/postEmployee")]
        public async Task<HttpResponseMessage> post(EmployeeDto employee)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (EmployeeRepo e = new EmployeeRepo())
                {
                    resultData = await e.CreateNewEmployee(employee);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }



        [HttpPut]
        //[ActionName("putEmployee")]
        //[Route("api/putEmployee")]
        public async Task<HttpResponseMessage> put(EmployeeDto employee)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (EmployeeRepo e = new EmployeeRepo())
                {
                    resultData = await e.UpdateEmployee(employee);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpDelete]
        //[ActionName("deleteEmployee")]
        //[Route("api/deleteEmployee")]
        public async Task<HttpResponseMessage> delete(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {//todo Check for workorder containing employee
                using (EmployeeRepo e = new EmployeeRepo())
                {
                    resultData = await e.DeleteEmployee(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }
    }
}
