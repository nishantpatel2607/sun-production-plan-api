﻿using production_plan_api.Models;
using production_plan_api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace production_plan_api.Controllers
{
    public class DesignationsController : ApiController
    {
        [HttpGet]
        //[ActionName("getDesignations")]
        //[Route("api/getDesignations")]
        public async Task<HttpResponseMessage> get()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<Designation> resultData = null;
            try
            {
                using (DesignationRepo e = new DesignationRepo())
                {
                    resultData = await e.GetAllDesignations();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }
         

        [HttpPost]
        //[ActionName("postDesignation")]
        //[Route("api/postDesignation")]
        public async Task<HttpResponseMessage> post(Designation designation)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (DesignationRepo e = new DesignationRepo())
                {
                    IEnumerable<int> recCount = await e.DesignationExists(designation.title);
                    if (recCount.ElementAt(0) == 0)
                    {
                        resultData = await e.CreateNewDesignation(designation.title);
                    }else
                    {
                        Msg = "Designation already exists.";
                        blnSuccess = false;
                    }
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        //[HttpPut]
        //[ActionName("putDesignation")]
        //[Route("api/putDesignation")]
        //To Call Put & Delete method we have to remove WebDAVModule added below block
        /*<modules runAllManagedModulesForAllRequests="true">
      
      <remove name="ApplicationInsightsWebTracking" />
      <add name="ApplicationInsightsWebTracking" type="Microsoft.ApplicationInsights.Web.ApplicationInsightsHttpModule, Microsoft.AI.Web" preCondition="managedHandler" />
      <remove name="WebDAVModule"/>
    </modules>*/
        public async Task<HttpResponseMessage> put(int id,Designation designation)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (DesignationRepo e = new DesignationRepo())
                {
                    resultData = await e.UpdateDesignation(designation.id, designation.title);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpDelete]
        //[ActionName("deleteDesignation")]
        //[Route("api/deleteDesignation")]
        public async Task<HttpResponseMessage> delete(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (DesignationRepo e = new DesignationRepo())
                {
                    resultData = await e.DeleteDesignation(id);
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    if (ex.InnerException.Message.Contains("foreign key"))
                    {
                        Msg = "Unable to delete title. It must be referenced by other records.";
                    } else
                    {
                        Msg = ex.Message;
                    }
                   
                } else
                {
                    Msg = ex.Message;
                }
               
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        //[HttpGet]
        //[ActionName("checkdesignation")]
        //[Route("api/checkdesignation")]
        //public async Task<HttpResponseMessage> checkdesignation(string title)
        //{
        //    bool blnSuccess = true;
        //    string Msg = string.Empty;
        //    IEnumerable<int> resultData = null;
        //    try
        //    {
        //        using (DesignationRepo e = new DesignationRepo())
        //        {
        //            resultData = await e.DesignationExists(title);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Msg = ex.Message;
        //        blnSuccess = false;
        //    }
        //    return new HttpResponseMessage()
        //    {
        //        Content = new JsonContent(new
        //        {
        //            Success = blnSuccess,
        //            Message = Msg,
        //            data = resultData
        //        })
        //    };
        //}



    }
}
