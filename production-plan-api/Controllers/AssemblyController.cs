﻿using production_plan_api.Models;
using production_plan_api.Models.DTO;
using production_plan_api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace production_plan_api.Controllers
{
    public class AssemblyController : ApiController
    {
        [HttpGet]
        [ActionName("getAssemblies")]
        [Route("api/getAssemblies")]
        public async Task<HttpResponseMessage> getAssemblies()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<AssemblyDto> resultData = null;
            try
            {
                using (AssemblyRepo e = new AssemblyRepo())
                {
                    resultData = await e.GetAllAssemblies();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("getFilterList")]
        [Route("api/assembly/getFilterList")]
        public async Task<HttpResponseMessage> getFilterList()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<FilterDto> resultData = null;
            try
            {
                using (AssemblyRepo e = new AssemblyRepo())
                {
                    resultData = await e.GetFilterList();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("getAssemblylist")]
        [Route("api/getAssemblylist")]
        public async Task<HttpResponseMessage> getAssemblylist()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<AssemblyListDto> resultData = null;
            try
            {
                using (AssemblyRepo e = new AssemblyRepo())
                {
                    resultData = await e.GetAssemblylist();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        //[ActionName("getAssembly")]
        //[Route("api/getAssembly")]
        public async Task<HttpResponseMessage> get(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            AssemblyDto assemblyDto = null;
            AssemblyDto[] resultData = new AssemblyDto[1];
            try
            {
                using (AssemblyRepo e = new AssemblyRepo())
                {
                    IEnumerable<Assembly> assembly = await e.GetAssembly(id);
                    IEnumerable<AssemblyDesignationDto> assemblyDesignation = await e.GetAssemblyDesignations(id);
                    IEnumerable<SubAssemblyDto> subAssemblies = await e.GetSubAssemblies(id);

                    assemblyDto = new AssemblyDto();
                    assemblyDto.assemblyDescription = assembly.ElementAt(0).assemblyDescription;
                    assemblyDto.assemblyName = assembly.ElementAt(0).assemblyName;
                    assemblyDto.duration = assembly.ElementAt(0).duration;
                    assemblyDto.id = assembly.ElementAt(0).id;
                    assemblyDto.assemblyDesignations = assemblyDesignation.ToArray();
                    assemblyDto.subAssemblies = subAssemblies.ToArray();
                    assemblyDto.categoryId = assembly.ElementAt(0).categoryId;
                    resultData[0] = assemblyDto;

                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPost]
        public async Task<HttpResponseMessage> post(AssemblyDto assembly)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (AssemblyRepo e = new AssemblyRepo())
                {
                    resultData = await e.CreateNewAssembly(assembly);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPut]
        public async Task<HttpResponseMessage> put(AssemblyDto assembly)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (AssemblyRepo e = new AssemblyRepo())
                {
                    resultData = await e.UpdateAssembly(assembly);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        public async Task<HttpResponseMessage> delete(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                //todo Check for workorder containing assembly
                using (AssemblyRepo e = new AssemblyRepo())
                {
                    resultData = await e.DeleteAssembly(id);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }
    }
}
