﻿using production_plan_api.Models.DTO;
using production_plan_api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace production_plan_api.Controllers
{
    public class WorkordersController : ApiController
    {
        [HttpGet]
        [ActionName("suitableemployees")]
        //[Route("api/workorders/suitableemployees")]
        public async Task<HttpResponseMessage> suitableemployees(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<WorkorderSuitableEmployeeDto> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.GetWorkorderSuitableEmployees(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("getFilterList")]
        [Route("api/Workorders/getFilterList")]
        public async Task<HttpResponseMessage> getFilterList()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<FilterDto> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.GetFilterList();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("list")]
        [Route("api/Workorders/list")]
        public async Task<HttpResponseMessage> list()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<WorkorderListDto> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.GetWorkorderList();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("getworkorder")]
        public async Task<HttpResponseMessage> get(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            WorkorderDto[] resultData = new WorkorderDto[1];
            try
            {
                IEnumerable<WorkorderDto> wo;
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    wo = await e.GetWorkorder(id);
                }
                resultData[0] = new WorkorderDto();
                resultData[0].assembly = wo.ElementAt(0).assembly;
                resultData[0].id = wo.ElementAt(0).id;
                resultData[0].machine = wo.ElementAt(0).machine;
                resultData[0].qty = wo.ElementAt(0).qty;
                resultData[0].workOrderDate = wo.ElementAt(0).workOrderDate;
                resultData[0].workOrderNo = wo.ElementAt(0).workOrderNo;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data =  resultData
                })
            };

        }


       

        [HttpGet]
        [ActionName("team")]
        //[Route("api/Workorders/team")]
        public async Task<HttpResponseMessage> team(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<WorkorderTeamDto> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.GetWorkorderTeam(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("plansindaterange")]
        //[Route("api/Workorders/plansindaterange")]
        public async Task<HttpResponseMessage> plansindaterange(string DateFrom, string DateTo, int FilterBy, string FilterValue)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<WorkorderPlanDto> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.GetWorkorderPlans(DateFrom, DateTo,FilterBy,FilterValue);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpPost]
       
        public async Task<HttpResponseMessage> post(WorkorderDto workorder)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.CreateWorkorder(workorder);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPut]
        public async Task<HttpResponseMessage> put(WorkorderDto workorder)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.UpdateWorkorder(workorder);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPost]
        [ActionName("addteam")]
        [Route("api/Workorders/addteam")]

        public HttpResponseMessage addteam(WorkorderTeamDto[] workorderTeam)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    e.AddWorkorderTeam(workorderTeam);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPost]
        [Route("api/Workorders/addplan")]
        public async Task<HttpResponseMessage> addplan(WorkorderPlanDto workorderPlan)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    resultData = await e.AddWorkorderPlan(workorderPlan);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPut]
        [Route("api/Workorders/editplan")]
        public HttpResponseMessage editplan(WorkorderPlanDto workorderPlan)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    e.UpdateWorkorderPlan(workorderPlan);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpDelete]
        public HttpResponseMessage delete(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    e.DeleteWorkorder(id);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }



        [HttpDelete]
        [Route("api/Workorders/deleteplan")]
        public HttpResponseMessage deleteplan(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    e.DeleteWorkorderPlan(id);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPut]
        [Route("api/Workorders/editactualplan")]
        public HttpResponseMessage editactualplan(WorkorderPlanDto workorderPlan)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (WorkorderRepo e = new WorkorderRepo())
                {
                    e.UpdateActualWorkorderPlan(workorderPlan);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

    }
}
