﻿using production_plan_api.Models;
using production_plan_api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace production_plan_api.Controllers
{
    public class MachineCategoriesController : ApiController
    {
        [HttpGet]
        //[ActionName("getMachineCategories")]
       // [Route("api/getMachineCategories")] 
        public async Task<HttpResponseMessage> get()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<MachineCategory> resultData = null;
            try
            {
                using (MachineCategoryRepo e = new MachineCategoryRepo())
                {
                    resultData = await e.GetAllMachineCategories();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpGet]
        //[ActionName("getMachineCategory")]
       // [Route("api/getMachineCategory")]
        public async Task<HttpResponseMessage> get(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<MachineCategory> resultData = null;
            try
            {
                using (MachineCategoryRepo e = new MachineCategoryRepo())
                {
                    resultData = await e.GetMachineCategory(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPost]
        //[ActionName("getMachineCategory")]
        //[Route("api/getMachineCategory")]
        public async Task<HttpResponseMessage> post(MachineCategory machineCategory)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (MachineCategoryRepo e = new MachineCategoryRepo())
                {
                    resultData = await e.CreateNewMachineCategory(machineCategory);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpPut]
        //[ActionName("getMachineCategory")]
        //[Route("api/getMachineCategory")]
        public async Task<HttpResponseMessage> put(MachineCategory machineCategory)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (MachineCategoryRepo e = new MachineCategoryRepo())
                {
                    resultData = await e.UpdateMachineCategory(machineCategory);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpDelete]
        //[ActionName("getMachineCategory")]
        //[Route("api/getMachineCategory")]
        public async Task<HttpResponseMessage> delete(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                //todo check for machine category used in machine

                using (MachineCategoryRepo e = new MachineCategoryRepo())
                {
                    resultData = await e.DeleteMachineCategory(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }
    }
}
