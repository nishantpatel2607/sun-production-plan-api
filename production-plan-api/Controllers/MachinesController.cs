﻿using production_plan_api.Models;
using production_plan_api.Models.DTO;
using production_plan_api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace production_plan_api.Controllers
{
    public class MachinesController : ApiController
    {
        [HttpGet]
       
        public async Task<HttpResponseMessage> get(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            MachineDto[] resultData = new MachineDto[1];
            try
            {
                
                using (MachineRepo e = new MachineRepo())
                {
                    IEnumerable<Machine> machine = await e.GetMachine(id);
                    IEnumerable<MachineDesignationDto> machineDesignations = await e.GetMachineDesignations(id);
                    IEnumerable<MachineAssemblyDto> machineAssemblies = await e.GetMachineAssemblies(id);
                    IEnumerable<MachineCategory> category = null;
                    using (MachineCategoryRepo me = new MachineCategoryRepo())
                    {
                        category = await me.GetMachineCategory(machine.ElementAt(0).categoryId);
                    }

                    MachineDto machineDto = new MachineDto();
                    machineDto.categoryId = machine.ElementAt(0).categoryId;
                    machineDto.categoryName = category.ElementAt(0).categoryName;
                    machineDto.doorType = machine.ElementAt(0).doorType;
                    machineDto.id = machine.ElementAt(0).id;
                    machineDto.installationType = machine.ElementAt(0).installationType;
                    machineDto.machineAssemblies = machineAssemblies.ToArray();
                    machineDto.machineDesignations = machineDesignations.ToArray();
                    machineDto.machineName = machine.ElementAt(0).machineName;
                    machineDto.machineType = machine.ElementAt(0).machineType;
                    machineDto.modelNo = machine.ElementAt(0).modelNo;
                    machineDto.orientation = machine.ElementAt(0).orientation;
                    machineDto.shape = machine.ElementAt(0).shape;
                    resultData[0] = machineDto;
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
       
        public async Task<HttpResponseMessage> get()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<MachineDto> resultData = null;
            try
            {
                using (MachineRepo e = new MachineRepo())
                {
                    resultData = await e.GetAllMachines();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("getFilterList")]
        [Route("api/machines/getFilterList")]
        public async Task<HttpResponseMessage> getFilterList()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<FilterDto> resultData = null;
            try
            {
                using (MachineRepo e = new MachineRepo())
                {
                    resultData = await e.GetFilterList();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpGet]
        [ActionName("list")]
        [Route("api/machines/list")]
        public async Task<HttpResponseMessage> list()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<MachineListDto> resultData = null;
            try
            {
                using (MachineRepo e = new MachineRepo())
                {
                    resultData = await e.GetMachineList();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPost]
        public async Task<HttpResponseMessage> post(MachineDto machine)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (MachineRepo e = new MachineRepo())
                {
                    resultData = await e.CreateNewMachine(machine);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPut]
        public async Task<HttpResponseMessage> put(MachineDto machine)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (MachineRepo e = new MachineRepo())
                {
                    resultData = await e.UpdateMachine(machine);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpDelete]
        public async Task<HttpResponseMessage> delete(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                //todo Check for workorder containing assembly
                using (MachineRepo e = new MachineRepo())
                {
                    resultData = await e.DeleteMachine(id);
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }
    }

}
