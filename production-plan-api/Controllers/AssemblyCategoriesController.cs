﻿using production_plan_api.Models;
using production_plan_api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace production_plan_api.Controllers
{
    public class AssemblyCategoriesController : ApiController
    {
        [HttpGet]
        public async Task<HttpResponseMessage> get()
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<AssemblyCategory> resultData = null;
            try
            {
                using (AssemblyCategoryRepo e = new AssemblyCategoryRepo())
                {
                    resultData = await e.GetAllAssemblyCategories();
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }
        [HttpGet]
        //[ActionName("getAssemblyCategory")]
        // [Route("api/getAssemblyCategory")]
        public async Task<HttpResponseMessage> get(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<AssemblyCategory> resultData = null;
            try
            {
                using (AssemblyCategoryRepo e = new AssemblyCategoryRepo())
                {
                    resultData = await e.GetAssemblyCategory(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }

        [HttpPost]
        //[ActionName("getAssemblyCategory")]
        //[Route("api/getAssemblyCategory")]
        public async Task<HttpResponseMessage> post(AssemblyCategory machineCategory)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (AssemblyCategoryRepo e = new AssemblyCategoryRepo())
                {
                    resultData = await e.CreateNewAssemblyCategory(machineCategory);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpPut]
        //[ActionName("getAssemblyCategory")]
        //[Route("api/getAssemblyCategory")]
        public async Task<HttpResponseMessage> put(AssemblyCategory machineCategory)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                using (AssemblyCategoryRepo e = new AssemblyCategoryRepo())
                {
                    resultData = await e.UpdateAssemblyCategory(machineCategory);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }


        [HttpDelete]
        //[ActionName("getAssemblyCategory")]
        //[Route("api/getAssemblyCategory")]
        public async Task<HttpResponseMessage> delete(int id)
        {
            bool blnSuccess = true;
            string Msg = string.Empty;
            IEnumerable<int> resultData = null;
            try
            {
                //todo check for machine category used in machine

                using (AssemblyCategoryRepo e = new AssemblyCategoryRepo())
                {
                    resultData = await e.DeleteAssemblyCategory(id);
                }

            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                blnSuccess = false;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(new
                {
                    Success = blnSuccess,
                    Message = Msg,
                    data = resultData
                })
            };
        }
    }
}
