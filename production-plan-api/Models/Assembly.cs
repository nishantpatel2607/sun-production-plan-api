﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models
{
    public class Assembly
    {
        public int id { get; set; }
        
        public string assemblyName { get; set; }

        public string assemblyDescription { get; set; }

        public int duration { get; set; }

        public int categoryId { get; set; }
    }
}