﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class MachineDto
    {
        public int id { get; set; }

        public string machineName { get; set; }

        public int categoryId { get; set; }

        public string categoryName { get; set; }

        public string modelNo { get; set; }

        public string installationType { get; set; }

        public string orientation { get; set; }

        public string shape { get; set; }

        public string doorType { get; set; }
        public string machineType { get; set; }

        public MachineAssemblyDto[] machineAssemblies { get; set; }

        public MachineDesignationDto[] machineDesignations { get; set; }
    }
}