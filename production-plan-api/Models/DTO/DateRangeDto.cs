﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class DateRangeDto
    {
        public string DateFrom;
        public string DateTo;
    }
}