﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class AssemblyDesignationDto
    {
        public int id { get; set; }

        public int assemblyId { get; set; }

        public int designationId { get; set; }

        public string title { get; set; }
    }
}