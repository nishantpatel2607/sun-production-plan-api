﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class AssemblyDto
    {
        public int id { get; set; } 

        public string assemblyName { get; set; }

        public string assemblyDescription { get; set; }

        public int duration { get; set; }
        public int categoryId { get; set; }

        public string categoryName { get; set; }

        public  SubAssemblyDto[] subAssemblies { get; set; }

        public AssemblyDesignationDto[] assemblyDesignations { get; set; }
    }
}