﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class EmployeeDto
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int designationId { get; set; }
        public string designation { get; set; }
        public string username { get; set; }
        public string password { get; set; }



    }
}