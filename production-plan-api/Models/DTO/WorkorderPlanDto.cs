﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class WorkorderPlanDto
    {
        public int id { get; set; }
        public int workOrderId { get; set; }
        public string workOrderNo { get; set; }
        public string machineName { get; set; }
        public int machineId { get; set; }
        public string assemblyName { get; set; }
        public int assemblyId { get; set; }
        public int qty { get; set; }
        public string plannedStartDate { get; set; } 
        public string plannedEndDate { get; set; } 
        public string plannedStartTime { get; set; } 
        public string plannedEndTime { get; set; } 
        public string actualStartDate { get; set; } 
        public string actualEndDate { get; set; } 
        public string actualStartTime { get; set; } 
        public string actualEndTime { get; set; } 
    }
}