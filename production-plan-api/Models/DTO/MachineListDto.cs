﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class MachineListDto 
    {
        public int id { get; set; }

        public string machineName { get; set; }
    }
}