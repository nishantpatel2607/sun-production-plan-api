﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class FilterDto
    {
        public int id { get; set; }
        public string itemValue { get; set; }
    }
}