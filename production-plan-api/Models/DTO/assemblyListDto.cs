﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class AssemblyListDto
    {
        public int id { get; set; }

        public string assemblyName { get; set; }

        public string categoryName { get; set; }

    }
}