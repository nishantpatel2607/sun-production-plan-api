﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class WorkorderListDto
    {
        public int id { get; set; }
        public string workOrderNo { get; set; }
        public string workOrderDate { get; set; }
        public string machineName { get; set; }
        public string assemblyName { get; set; }
        public int qty { get; set; }
        
    }
}