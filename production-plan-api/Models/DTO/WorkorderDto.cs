﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models.DTO
{
    public class WorkorderDto
    {
        public int id { get; set; }
        public string workOrderNo { get; set; }
        public string workOrderDate { get; set; }
        public MachineListDto machine { get; set; }
        public AssemblyListDto assembly { get; set; }
        public int qty { get; set; }

    }

    public class WorkorderQueryDto
    {
        public int id { get; set; }
        public string workOrderNo { get; set; }
        public string workOrderDate { get; set; }
        public string machine { get; set; }
        public int machineId { get; set; }
        public string assembly { get; set; }
        public int assemblyId { get; set; }
        public int qty { get; set; }

    }
}