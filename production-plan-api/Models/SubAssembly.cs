﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models
{
    public class SubAssembly
    {

        public int id { get; set; }

        public int assemblyId { get; set; }

        public int? subAssemblyId { get; set; }

        public int qty { get; set; }

        public int sequenceNo { get; set; }

    }
}