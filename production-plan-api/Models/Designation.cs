﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models
{
    public class Designation
    {
        public int id { get; set; }
        public string title { get; set; }
    }
}