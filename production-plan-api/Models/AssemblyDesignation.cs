﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Models
{
    public class AssemblyDesignation
    {
        public int id { get; set; }

        public int assemblyId { get; set; }

        public int designationId { get; set; }

    }
}