﻿using Dapper;
using production_plan_api.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace production_plan_api.Repository
{
    public class DesignationRepo : BaseRepository, IDisposable
    {
        public DesignationRepo() : base()
        {

        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public async Task<IEnumerable<Designation>> GetAllDesignations()
        {
            string query = "select * from designations order by title";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<Designation>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }


        public async Task<IEnumerable<int>> DesignationExists(string title)
        {
            string query = "select count(*) from designations where title = '" + Globals.NormalizeString(title) + "'";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }

        public async Task<IEnumerable<int>> CreateNewDesignation(string title) {
            string query = "insert into designations (title) values ('" + Globals.NormalizeString(title) + "');SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }

        public async Task<IEnumerable<int>> UpdateDesignation(int id, string title)
        {
            string query = "Update designations  set title = '" + Globals.NormalizeString(title) + "' where id = " + id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }


        public async Task<IEnumerable<int>> DeleteDesignation( int id) {
            string query = "delete from designations where id = " + id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }
    }
}