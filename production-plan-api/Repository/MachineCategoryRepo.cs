﻿using Dapper;
using production_plan_api.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace production_plan_api.Repository
{
    public class MachineCategoryRepo : BaseRepository, IDisposable
    { 
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public MachineCategoryRepo() : base()
        {

        }

        public async Task<IEnumerable<MachineCategory>> GetAllMachineCategories()
        {
            string query = "select id, categoryName from machinecategories order by categoryName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<MachineCategory>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<MachineCategory>> GetMachineCategory(int id)
        {
            string query = "select id, categoryName from machinecategories where id = " + id;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<MachineCategory>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<int>> CreateNewMachineCategory(MachineCategory category)
        {
            string query = "insert into machinecategories (categoryName) values ('" + Globals.NormalizeString(category.categoryName) + "');SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                category.id = list.ElementAt(0);
                return list;
            });
        }

        public async Task<IEnumerable<int>> UpdateMachineCategory(MachineCategory category)
        {
            string query = "update machinecategories set categoryName = '" + Globals.NormalizeString(category.categoryName) + "' where id = " + category.id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }

        public async Task<IEnumerable<int>> IsMachineCategoryUsed(int id)
        {
            string query = "select count(*) machines where categoryid = " + id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }
        public async Task<IEnumerable<int>> DeleteMachineCategory(int id)
        {
            string query = "delete from machinecategories where id = " + id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }

    }
}