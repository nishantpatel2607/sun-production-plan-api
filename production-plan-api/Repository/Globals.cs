﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace production_plan_api.Repository
{
    public static class Globals
    {
        public static string ConvertDateFormatToMySQLFormat(string Dt)
        {
            string[] arDt = Dt.Split(new char[] { '/' });
            string resultDt = arDt[2] + "-" + arDt[1] + "-" + arDt[0];
            return resultDt;
        }

        public static string NormalizeString(string str)
        {
            return str.Replace("'", "''");
        }
    }
}