﻿using Dapper;
using production_plan_api.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace production_plan_api.Repository
{
    public class EmployeeRepo : BaseRepository, IDisposable
    {
        public EmployeeRepo() : base()
        {

        }
        public void Dispose()
        {
            //throw new NotImplementedException();
        }



        //get all employees
        public async Task<IEnumerable<EmployeeDto>> GetAllEmployees(bool includeUsernamePassword = true)
        {
            string query = "";
            if (includeUsernamePassword)
            {
                query = "select a.*, b.title as designation from employees a, designations b where a.designationId = b.Id order by a.firstname";
            }
            else
            {
                query = "select a.id,a.firstname,a.lastname,a.designationId,b.title as designation from employees a, designations b where a.designationId = b.Id order by a.firstname";
            }
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<EmployeeDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }


        public async Task<IEnumerable<FilterDto>> GetFilterList()
        {
            string query = "select id as Id, CONCAT(firstName , ' ' , lastName) as ItemValue from employees order by firstName,lastName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<FilterDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        //get employee record
        public async Task<IEnumerable<EmployeeDto>> GetEmployeeRecord(int id)
        {
            string query = "select a.*, b.title as designation from employees a, designations b where a.designationId = b.Id and a.id = " + id;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<EmployeeDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }


        //create employee record
        public async Task<IEnumerable<int>> CreateNewEmployee(EmployeeDto employee)
        {
            string query = "insert into employees (firstname,lastname,designationId,username,password) values ('" + Globals.NormalizeString(employee.firstName) + "','" + Globals.NormalizeString(employee.lastName) + "'," + employee.designationId + ",'" + Globals.NormalizeString(employee.username) + "','" + Globals.NormalizeString(employee.password) + "');SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }
        //update employee record

        public async Task<IEnumerable<int>> UpdateEmployee(EmployeeDto employee)
        {
            string query = "update employees set firstname ='" + Globals.NormalizeString(employee.firstName) + "',lastname = '" + Globals.NormalizeString(employee.lastName) + "', designationId = " + employee.designationId + ",username = '" + Globals.NormalizeString(employee.username) + "',password = '" + Globals.NormalizeString(employee.password) + "' where id = " + employee.id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }

        //delete employee record

        public async Task<IEnumerable<int>> DeleteEmployee(int id)
        {
            string query = "delete from employees where id = " + id;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

    }
}