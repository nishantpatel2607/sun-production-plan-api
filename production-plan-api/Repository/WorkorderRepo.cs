﻿using Dapper;
using production_plan_api.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace production_plan_api.Repository
{
    public class WorkorderRepo : BaseRepository, IDisposable
    {
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public WorkorderRepo() : base()
        {

        }

        public async Task<IEnumerable<WorkorderListDto>> GetWorkorderList()
        {
            string query = "select a.id, a.workOrderNo, DATE_FORMAT(a.workOrderDate,'%d/%m/%Y') as workOrderDate ,a.qty, CONCAT( b.machineName,' (', b.modelNo ,')') as machineName, c.assemblyName from workorders a left join machines b on a.machineid = b.id left join assemblies c on a.assemblyid = c.id order by a.id";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<WorkorderListDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<FilterDto>> GetFilterList()
        {
            string query = "select id as Id, CONCAT(workOrderNo, ' (' , DATE_FORMAT(workOrderDate,'%d/%m/%Y') , ')') as ItemValue from workorders order by workOrderNo";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<FilterDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<WorkorderDto>> GetWorkorder(int id)
        {
            WorkorderDto[] wo = new WorkorderDto[1];
            string query = "select a.id,a.workOrderNo, DATE_FORMAT(a.workOrderDate,'%d/%m/%Y') as workOrderDate , a.machineid, a.assemblyid ,a.qty, b.machineName as machine, c.assemblyName as assembly from workorders a left join machines b on a.machineid = b.id left join assemblies c on a.assemblyid = c.id where a.id = " + id;

            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<WorkorderQueryDto>(sql: query, commandType: CommandType.Text);
                //return list;
                AssemblyListDto assembly = new AssemblyListDto();
                assembly.id = list.ElementAt(0).assemblyId;
                assembly.assemblyName = list.ElementAt(0).assembly;

                MachineListDto machine = new MachineListDto();
                machine.id = list.ElementAt(0).machineId;
                machine.machineName = list.ElementAt(0).machine;
                wo[0] = new WorkorderDto();
                wo[0].assembly = new AssemblyListDto();
                wo[0].assembly = assembly;
                wo[0].id = list.ElementAt(0).id;
                wo[0].machine = new MachineListDto();
                wo[0].machine = machine;
                wo[0].qty = list.ElementAt(0).qty;
                wo[0].workOrderDate = list.ElementAt(0).workOrderDate;
                wo[0].workOrderNo = list.ElementAt(0).workOrderNo;

            });
            return wo;
        }

        public async Task<IEnumerable<WorkorderSuitableEmployeeDto>> GetWorkorderSuitableEmployees(int id)
        {
            var list = GetWorkorder(id);

            string query = "";

            WorkorderDto wo = list.Result.ElementAt(0);
            if (wo.assembly.id > 0)
            {
                query = "select a.id, a.firstName, a.lastName, b.title as designation from employees a, designations b, assemblydesignations c where a.designationid = b.id and a.designationid = c.designationid and c.assemblyid = " + wo.assembly.id + " and a.id not in (select employeeid from workorderteam where workorderid =" + wo.id +")";
            }
            else if (wo.machine.id > 0)
            {
                query = "select a.id, a.firstName, a.lastName, b.title as designation from employees a, designations b, machinedesignations c where a.designationid = b.id and a.designationid = c.designationid and c.machineid = " + wo.machine.id + " and a.id not in (select employeeid from workorderteam where workorderid =" + wo.id + ")"; 
            }

            return await WithConnection(async c =>
            {
                var result = await c.QueryAsync<WorkorderSuitableEmployeeDto>(sql: query, commandType: CommandType.Text);
                return result;
            });

        }

        public async Task<IEnumerable<WorkorderTeamDto>> GetWorkorderTeam(int id)
        {

            string query = "select a.id as employeeId, a.firstName, a.lastName, b.title as designation, c.workOrderId from employees a, designations b, workorderteam c where a.designationid = b.id and a.id = c.employeeid and c.workorderid = " + id;


            return await WithConnection(async c =>
            {
                var result = await c.QueryAsync<WorkorderTeamDto>(sql: query, commandType: CommandType.Text);
                return result;
            });

        }

        public async Task<IEnumerable<WorkorderPlanDto>> GetWorkorderPlans(string startDate, string endDate, int filterBy, string filterValue)
        {
            //filterBy : 0-No filter, 1 - Workorder id, 2 - employee id, 3-assembly id, 4 - machine id
            string query = "select d.id,d.workOrderId,a.workOrderNo, a.qty, b.machineName, b.id as machineId, c.assemblyName, c.id as assemblyId,DATE_FORMAT(d.plannedStartDate,'%Y-%m-%d') as plannedStartDate , DATE_FORMAT(d.plannedEndDate,'%Y-%m-%d') as plannedEndDate, d.plannedStartTime, d.plannedEndTime, DATE_FORMAT(d.actualStartDate,'%Y-%m-%d') as actualStartDate, DATE_FORMAT(d.actualEndDate,'%Y-%m-%d') as actualEndDate, d.actualStartTime, d.actualEndTime from workorders a left join machines b on a.machineid = b.id left join assemblies c on a.assemblyid = c.id join workorderplan d on a.id = d.workorderid where d.plannedStartDate >= '" + startDate + "' and d.plannedStartDate <= '" + endDate + "'" ;
            if (filterBy == 1)
            {
                query += " and a.Id = " + filterValue;
            } else if (filterBy == 2)
            {
                query += " and a.id in (select workorderid from workorderteam where employeeid = " + filterValue + ")";
            } else if (filterBy == 3)
            {
                query += " and c.Id = " + filterValue;

            } else if (filterBy == 4)
            {
                query += " and b.Id = " + filterValue;
            }

            return await WithConnection(async c =>
            {
                var result = await c.QueryAsync<WorkorderPlanDto>(sql: query, commandType: CommandType.Text);
                return result;
            });
        }

        public async Task<IEnumerable<int>> CreateWorkorder(WorkorderDto workorder)
        {
            workorder.workOrderDate = Globals.ConvertDateFormatToMySQLFormat(workorder.workOrderDate);
            string query = "insert into workorders (workorderNo,workorderdate,machineid,assemblyid,qty) values ('" +     Globals.NormalizeString(workorder.workOrderNo) + "','" + workorder.workOrderDate + "', " + workorder.machine.id + "," + workorder.assembly.id + "," + workorder.qty + ");SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<int>> UpdateWorkorder(WorkorderDto workorder)
        {
            workorder.workOrderDate = Globals.ConvertDateFormatToMySQLFormat(workorder.workOrderDate);
            string query = "Update workorders set workorderNo = '" + Globals.NormalizeString(workorder.workOrderNo) + "', workorderdate ='" + workorder.workOrderDate + "', machineid =" + workorder.machine.id + ",assemblyid =" + workorder.assembly.id + ", qty =" + workorder.qty + " where id = " + workorder.id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async void AddWorkorderTeam(WorkorderTeamDto[] workorderTeam)
        {
            //delete all team members 
            string delquery = "delete from workorderteam where workorderid = " + workorderTeam[0].workOrderId;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: delquery, commandType: CommandType.Text);

            });

            for (int i = 0; i < workorderTeam.Length; i++)
            {
                string query = "insert into workorderteam (workorderid,employeeid) values (" + workorderTeam[i].workOrderId + "," + workorderTeam[i].employeeId + ")".Replace("'","''");

                await WithConnection(async c =>
                {
                    var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                });
            }
        }

        public async Task<IEnumerable<int>> AddWorkorderPlan(WorkorderPlanDto workorderPlan)
        {
            string query = "insert into workorderplan (workOrderId,plannedStartDate, plannedEndDate, plannedStartTime, plannedEndTime) values (" + workorderPlan.workOrderId + ",'" + workorderPlan.plannedStartDate + "', '" + workorderPlan.plannedEndDate + "','" + workorderPlan.plannedStartTime + "','" + workorderPlan.plannedEndTime + "'); SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async void UpdateWorkorderPlan(WorkorderPlanDto workorderPlan)
        {
            string query = "Update workorderplan set plannedStartDate = '" + workorderPlan.plannedStartDate + "', plannedEndDate = '" + workorderPlan.plannedStartDate + "', plannedStartTime = '" + workorderPlan.plannedStartTime + "', plannedEndTime = '" + workorderPlan.plannedEndTime + "' where id = " + workorderPlan.id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
                
            });
        }

        public async void DeleteWorkorderPlan(int workorderPlanId)
        {
            string query = "Delete from workorderplan where id = " + workorderPlanId;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });
        }

        public async void UpdateActualWorkorderPlan(WorkorderPlanDto workorderPlan)
        {
            string query = "Update workorderplan set actualStartDate = '" + workorderPlan.actualStartDate + "', actualEndDate = '" + workorderPlan.actualStartDate + "', actualStartTime = '" + workorderPlan.actualStartTime + "', actualEndTime = '" + workorderPlan.actualEndTime + "' where id = " + workorderPlan.id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
            });
        }


        public async void DeleteWorkorder(int workorderId)
        {
            string query = "Delete from workorderplan where workorderid = " + workorderId;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });
            query = "Delete from workorderteam where workorderid = " + workorderId;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            query = "Delete from workorders where id = " + workorderId;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });
        }
    }



}
