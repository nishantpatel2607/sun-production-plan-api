﻿using Dapper;
using production_plan_api.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace production_plan_api.Repository
{
    public class AssemblyCategoryRepo : BaseRepository, IDisposable
    {
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public AssemblyCategoryRepo() : base()
        {

        }

        public async Task<IEnumerable<AssemblyCategory>> GetAllAssemblyCategories()
        {
            string query = "select id, categoryName from assemblycategories order by categoryName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<AssemblyCategory>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<AssemblyCategory>> GetAssemblyCategory(int id)
        {
            string query = "select id, categoryName from assemblycategories where id = " + id;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<AssemblyCategory>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<int>> CreateNewAssemblyCategory(AssemblyCategory category)
        {
            string query = "insert into assemblycategories (categoryName) values ('" + Globals.NormalizeString(category.categoryName) + "');SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                category.id = list.ElementAt(0);
                return list;
            });
        }

        public async Task<IEnumerable<int>> UpdateAssemblyCategory(AssemblyCategory category)
        {
            string query = "update assemblycategories set categoryName = '" + Globals.NormalizeString(category.categoryName) + "' where id = " + category.id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }

        public async Task<IEnumerable<int>> IsAssemblyCategoryUsed(int id)
        {
            string query = "select count(*) assemblys where categoryid = " + id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }
        public async Task<IEnumerable<int>> DeleteAssemblyCategory(int id)
        {
            string query = "delete from assemblycategories where id = " + id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                return list;
            });
        }
    }
}