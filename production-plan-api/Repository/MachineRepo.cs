﻿using Dapper;
using production_plan_api.Models;
using production_plan_api.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace production_plan_api.Repository
{
    public class MachineRepo : BaseRepository, IDisposable
    {
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public MachineRepo():base()
        {  }

        //get all machines
        public async Task<IEnumerable<MachineDto>> GetAllMachines() 
        {
            string query = "select a.*,b.categoryName from machines a, machinecategories b where a.categoryid = b.id order by machineName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<MachineDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<FilterDto>> GetFilterList()
        {
            string query = "select id as Id, CONCAT(machineName,' (', modelNo, ')') as ItemValue from machines order by machineName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<FilterDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<MachineListDto>> GetMachineList()
        {
            string query = "select id,CONCAT( machineName,' (', modelNo ,')') as machineName from machines order by machineName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<MachineListDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }


        //get machine
        public async Task<IEnumerable<Machine>> GetMachine(int id)
        {
            string query = "select * from machines where id = " + id;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<Machine>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }


        //get machine designations
        public async Task<IEnumerable<MachineDesignationDto>> GetMachineDesignations(int id)
        {
            string query = "select machineid, designationid, title from machinedesignations a, designations b where a.designationid = b.id and  machineid = " + id;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<MachineDesignationDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        //get sub assemblies for given machine id
        public async Task<IEnumerable<MachineAssemblyDto>> GetMachineAssemblies(int id)
        {
            string query = "select machineid, subassemblyid, qty, b.assemblyName as subassemblyname, sequenceNo from machineassemblies a, assemblies b where a.machineid = " + id + " and a.subassemblyid = b.id order by sequenceNo";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<MachineAssemblyDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        //save machine
        public async Task<IEnumerable<int>> CreateNewMachine(MachineDto machine) 
        {
            string query = "insert into machines (machinename,categoryid,modelNo,installationType,orientation,shape,doortype,machinetype) values ('" + machine.machineName + "'," + machine.categoryId + ", '" + machine.modelNo + "','" + machine.installationType + "','" + machine.orientation + "','" + machine.shape + "','" + machine.doorType + "','" + machine.machineType + "');SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                machine.id = list.ElementAt(0);
                this.AddDesignationsInMachine(machine);
                this.AddSubAssembliesInMachine(machine);
                return list;
            });
        }


        public async Task<IEnumerable<int>> UpdateMachine(MachineDto machine)
        {
            string query = "Update machines set machinename = '" + machine.machineName + "', categoryid =" + machine.categoryId + ", modelNo = '" + machine.modelNo + "',installationType = '" + machine.installationType + "',orientation = '" + machine.orientation + "',shape = '" + machine.shape + "',doortype = '" + machine.doorType + "',machinetype = '" + machine.machineType + "' where id = " + machine.id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                
                this.AddDesignationsInMachine(machine);
                this.AddSubAssembliesInMachine(machine);
                return list;
            });
        }

        public async void AddDesignationsInMachine(MachineDto machine)
        {
            string query = "delete from machineDesignations where machineId = " + machine.id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            for (int i = 0; i < machine.machineDesignations.Length; i++)
            {
                query = "Insert into machineDesignations (machineid,designationid) values (" + machine.id + "," + machine.machineDesignations[i].designationId + ")";
                await WithConnection(async c =>
                {
                    var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                });
            }
        }


        //save sub assemblies = overwrite existing records
        public async void AddSubAssembliesInMachine(MachineDto machine)
        {
            string query = "delete from machineassemblies where machineId = " + machine.id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            for (int i = 0; i < machine.machineAssemblies.Length; i++)
            {
                query = "Insert into machineassemblies (machineid,subassemblyid, qty, sequenceNo) values (" + machine.id + "," + machine.machineAssemblies[i].subAssemblyId + "," + machine.machineAssemblies[i].qty + "," + machine.machineAssemblies[i].sequenceNo + ")";
                await WithConnection(async c =>
                {
                    var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                });
            }
        }

        public async Task<IEnumerable<int>> DeleteMachine(int id)
        {

            string query = "delete from machineassemblies where machineId = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            

            query = "delete from machinedesignations where machineId = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            query = "delete from machines where Id = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
                return list;
            });
            return null;

        }
    }
}