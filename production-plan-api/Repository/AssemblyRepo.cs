﻿using Dapper;
using production_plan_api.Models;
using production_plan_api.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace production_plan_api.Repository
{
    public class AssemblyRepo : BaseRepository, IDisposable
    {
        public AssemblyRepo() : base()
        {
                    
        }
        public void Dispose()
        {
           // throw new NotImplementedException();
        }

        //get all assemblies
        public async Task<IEnumerable<AssemblyDto>> GetAllAssemblies() 
        {
            string query = "select a.*, b.categoryName from assemblies a left join assemblycategories b  on a.categoryId = b.id order by assemblyName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<AssemblyDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        public async Task<IEnumerable<FilterDto>> GetFilterList()
        {
            string query = "select id as Id, assemblyName as ItemValue from assemblies order by assemblyName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<FilterDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }


        //get top level assemblies
        /*public async Task<IEnumerable<Assembly>> GetTopLevelAssemblies()
        {
            string query = "select * from assemblies where id not in (select distinct subassemblyid from subassemblies)  order by assemblyName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<Assembly>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }*/


        //get assembly list (id and name)
        public async Task<IEnumerable<AssemblyListDto>> GetAssemblylist() 
        {
            string query = "select a.id, assemblyName, b.categoryName from assemblies a, assemblycategories b  where a.categoryid = b.id order by assemblyName";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<AssemblyListDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        //get assembly designations
        public async Task<IEnumerable<AssemblyDesignationDto>> GetAssemblyDesignations(int id)
        {
            string query = "select assemblyid, designationid, title from assemblydesignations a, designations b where a.designationid = b.id and  assemblyid = " + id;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<AssemblyDesignationDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        //get sub assemblies for given assembly id
        public async Task<IEnumerable<SubAssemblyDto>> GetSubAssemblies(int id)
        {
            string query = "select assemblyid, subassemblyid,assemblyid,qty,duration*qty as duration, b.assemblyName as subassemblyname, sequenceNo from subassemblies a, assemblies b where a.assemblyid = " + id + " and a.subassemblyid = b.id order by sequenceNo";

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<SubAssemblyDto>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }


        //get assembly
        public async Task<IEnumerable<Assembly>> GetAssembly(int id)
        {
            string query = "select * from assemblies where id = " + id ;

            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<Assembly>(sql: query, commandType: CommandType.Text);
                return list;
            });
        }

        //save assembly
        public async Task<IEnumerable<int>> CreateNewAssembly(AssemblyDto assembly)
        {
            string query = "insert into assemblies (assemblyname,assemblydescription,duration,categoryid) values ('" + Globals.NormalizeString(assembly.assemblyName) + "','" + Globals.NormalizeString(assembly.assemblyDescription) + "'," + assembly.duration + "," + assembly.categoryId + ");SELECT LAST_INSERT_ID();";
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                assembly.id = list.ElementAt(0);
                this.AddDesignationsInAssembly(assembly);
                this.AddSubAssembliesInAssembly(assembly);
                return list;
            });
        }

        public async Task<IEnumerable<int>> UpdateAssembly(AssemblyDto assembly)
        {
            string query = "update assemblies set assemblyname ='" + Globals.NormalizeString(assembly.assemblyName) + "', assemblydescription = '" + Globals.NormalizeString(assembly.assemblyDescription) + "', duration = " + assembly.duration + ", categoryId = " + assembly.categoryId + " where id = " + assembly.id;
            return await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                //assembly.id = list.ElementAt(0);
                this.AddDesignationsInAssembly(assembly);
                this.AddSubAssembliesInAssembly(assembly);
                return list;
            });
        }

        //save assembly designations - overwrite existing records
        public async void AddDesignationsInAssembly(AssemblyDto assembly)
        {
            string query = "delete from assemblyDesignations where assemblyId = " + assembly.id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            for (int i =0; i < assembly.assemblyDesignations.Length; i++)
            {
                query = "Insert into assemblyDesignations (assemblyid,designationid) values (" + assembly.id + "," + assembly.assemblyDesignations[i].designationId + ")";
                await WithConnection(async c =>
                {
                    var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                });
            }
        }


        //save sub assemblies = overwrite existing records
        public async void AddSubAssembliesInAssembly(AssemblyDto assembly)
        {
            string query = "delete from subassemblies where assemblyId = " + assembly.id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            for (int i = 0; i < assembly.subAssemblies.Length; i++)
            {
                query = "Insert into subassemblies (assemblyid,subassemblyid, qty,sequenceNo) values (" + assembly.id + "," + assembly.subAssemblies[i].subAssemblyId + "," + assembly.subAssemblies[i].qty + "," + assembly.subAssemblies[i].sequenceNo + ")";
                await WithConnection(async c =>
                {
                    var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

                });
            }
        }

        public async Task<IEnumerable<int>> DeleteAssembly(int id)
        {
            
            string query = "delete from subassemblies where assemblyId = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            query = "delete from subassemblies where subassemblyId = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            query = "delete from machineassemblies where subassemblyId = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            query = "delete from assemblydesignations where assemblyId = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);

            });

            query = "delete from assemblies where Id = " + id;
            await WithConnection(async c =>
            {
                var list = await c.QueryAsync<int>(sql: query, commandType: CommandType.Text);
                return list;
            });
            return null;
            
        }

    }
}